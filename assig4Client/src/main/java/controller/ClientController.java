package controller;

import entity.Package;
import entity.PackageTable;
import entity.Route;
import entity.RouteDTO;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import service.AdministratorService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClientController {

    private int id;
    private AdministratorService administratorService;

    @FXML
    private TableView<PackageTable> tablePackage;
    @FXML
    private TableColumn<PackageTable, String> tableSender;
    @FXML
    private TableColumn<PackageTable, String> tableReceiver;
    @FXML
    private TableColumn<PackageTable, String> tableName;
    @FXML
    private TableColumn<PackageTable, String> tableDescription;
    @FXML
    private TableColumn<PackageTable, String> tableCity;
    @FXML
    private TableColumn<PackageTable, String> tableTracking;
    @FXML
    private TableColumn<PackageTable, String> tableId;
    @FXML
    private TextField idField;
    @FXML
    private TextArea routesArea;
    @FXML
    private TextField nameField;

    public ClientController(int id) {
        this.id = id;
        try {
            URL url = new URL("http://localhost:8080/administratorService?wsdl");
            QName qname = new QName("http://service/", "AdministratorServiceImplService");
            Service service = Service.create(url, qname);
            administratorService = service.getPort(AdministratorService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void listAllUserPackages() {
        List<Package> packages = administratorService.findUserPackages(this.id);
        tableId.setCellValueFactory(new PropertyValueFactory<PackageTable, String>("id"));
        tableSender.setCellValueFactory(new PropertyValueFactory<PackageTable, String>("sender"));
        tableReceiver.setCellValueFactory(new PropertyValueFactory<PackageTable, String>("receiver"));
        tableName.setCellValueFactory(new PropertyValueFactory<PackageTable, String>("name"));
        tableDescription.setCellValueFactory(new PropertyValueFactory<PackageTable, String>("description"));
        tableCity.setCellValueFactory(new PropertyValueFactory<PackageTable, String>("city"));
        tableTracking.setCellValueFactory(new PropertyValueFactory<PackageTable, String>("tracking"));
        List<PackageTable> tablePackages = new ArrayList<PackageTable>();
        for (Package pack : packages) {
            PackageTable packageTable = new PackageTable();
            packageTable.setId(String.valueOf(pack.getId()));
            packageTable.setSender(pack.getSender().getUsername());
            packageTable.setReceiver(pack.getReceiver().getUsername());
            packageTable.setName(pack.getName());
            packageTable.setDescription(pack.getDescription());
            packageTable.setCity(pack.getSenderCity().getName() + " " + pack.getDetinationCity().getName());
            packageTable.setTracking(String.valueOf(pack.isTracking()));
            tablePackages.add(packageTable);
        }
        tablePackage.getItems().setAll(tablePackages);

    }

    public void displayPackages() {
        try {
            int id = Integer.parseInt(idField.getText());
            List<RouteDTO> routes = administratorService.findPackageRoutes(id);
            if (routes.isEmpty()) showAlert("Package is not tracked");
            else {
                StringBuilder sb = new StringBuilder();
                for (RouteDTO routeDTO : routes) {
                    sb.append(routeDTO.getCity().getName()).append(" ").append(routeDTO.getDate()).append("\n");
                }
                routesArea.setText(sb.toString());
            }
        } catch (NumberFormatException e) {
            showAlert("Id must be a number!");
        }
    }

    public void findPackageByName() {
        String name = nameField.getText();
        List<Package> packages = administratorService.findUserPackages(this.id)
                .stream().filter(p -> p.getName().equals(name)).collect(Collectors.toList());
        if (packages.isEmpty()) routesArea.setText("No such package");
        else {
            Package pack = packages.get(0);
            routesArea.setText(pack.getName() + " " + pack.getDescription() + "\n"
                    + pack.getSenderCity().getName() + " " + pack.getDetinationCity().getName());
        }
    }

    private void showAlert(String s) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(s);
        alert.showAndWait();
    }

}
