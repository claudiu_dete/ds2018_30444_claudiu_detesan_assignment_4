﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServerAssig4.entity
{
    [Table("package")]
    public class Package
    {
        [Key]
        public int Id;
        public User Sender;
        public User Receiver;
        public String Name;
        public String Description;
        public City SenderCity;
        public City DestinationCity;
        public bool Tracking;

        public Package()
        {

        }

    }
}