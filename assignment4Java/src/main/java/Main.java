import dao.CityDao;
import dao.PackageDao;
import dao.RouteDao;
import dao.UserDao;
import service.AdministratorServiceImpl;

import javax.xml.ws.Endpoint;

public class Main {

    public static void main(String[] args) {

        Endpoint.publish(
                "http://localhost:8080/administratorService",
                new AdministratorServiceImpl(new PackageDao(),new RouteDao(),new CityDao(),new UserDao()));
    }
}
