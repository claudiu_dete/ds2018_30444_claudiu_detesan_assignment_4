package service;

import entity.*;
import entity.Package;
import entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface AdministratorService {

    @WebMethod
    int createPackage(Package pack);

    @WebMethod
    void deletePackage(int id);

    @WebMethod
    void addRouteToPackage(int id, String date, String cityName);

    @WebMethod
    List<Package> getAllPackages();

    @WebMethod
    List<User> getAllUsers();

    @WebMethod
    List<City> getAllCities();

    @WebMethod
    List<Package> findUserPackages(int id);

    @WebMethod
    List<RouteDTO> findPackageRoutes(int id);

}
