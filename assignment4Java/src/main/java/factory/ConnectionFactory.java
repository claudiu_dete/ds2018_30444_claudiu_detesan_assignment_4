package factory;

import entity.City;
import entity.Package;
import entity.Route;
import entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class ConnectionFactory {

    private static ConnectionFactory instance;
    private SessionFactory sessionFactory;

    public static ConnectionFactory getInstance() {
        if (instance == null) {
            instance = new ConnectionFactory();
        }
        return instance;
    }

    private ConnectionFactory() {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(City.class);
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Package.class);
        configuration.addAnnotatedClass(Route.class);
        ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        SessionFactory sess = configuration.buildSessionFactory(serviceRegistry);
        this.sessionFactory = sess;
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }
}
