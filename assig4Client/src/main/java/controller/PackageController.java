package controller;

import entity.City;
import entity.Package;
import entity.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import service.AdministratorService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class PackageController implements Initializable {

    private AdministratorService administratorService;

    @FXML
    private ComboBox sendCombo;
    @FXML
    private ComboBox receCombo;
    @FXML
    private ComboBox scityCombo;
    @FXML
    private ComboBox rcityCombo;
    @FXML
    private TextField nameField;
    @FXML
    private TextField descriptionField;
    @FXML
    private Button backBtn;

    public void initialize(URL location, ResourceBundle resources) {
        initializeController();
        initComboBox();
    }

    private void initializeController() {
        try {
            URL url = new URL("http://localhost:8080/administratorService?wsdl");
            QName qname = new QName("http://service/", "AdministratorServiceImplService");
            Service service = Service.create(url, qname);
            administratorService = service.getPort(AdministratorService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initComboBox() {
        List<User> users = administratorService.getAllUsers();
        List<String> usernames = new ArrayList<String>();
        for (User user : users) {
            usernames.add(user.getUsername());
        }
        sendCombo.getItems().addAll(usernames);
        receCombo.getItems().addAll(usernames);

        List<City> cities = administratorService.getAllCities();
        for (City city : cities) {
            rcityCombo.getItems().add(city.getName());
            scityCombo.getItems().add(city.getName());
        }
    }

    public void addPackage() {
        String name = nameField.getText();
        String description = descriptionField.getText();
        String senderName = sendCombo.getSelectionModel().getSelectedItem().toString();
        String receiverName = receCombo.getSelectionModel().getSelectedItem().toString();
        String senderCityName = rcityCombo.getSelectionModel().getSelectedItem().toString();
        String receiverCityName = scityCombo.getSelectionModel().getSelectedItem().toString();
        Package pack = new Package(new User(senderName), new User(receiverName), name, description, new City(senderCityName),
                new City(receiverCityName), false);
        administratorService.createPackage(pack);
    }

    public void back() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/AdministratorView.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage window = (Stage) backBtn.getScene().getWindow();
            window.setScene(scene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
