package service;

import dao.CityDao;
import dao.PackageDao;
import dao.RouteDao;
import dao.UserDao;
import entity.*;
import entity.Package;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebService(endpointInterface = "service.AdministratorService")
public class AdministratorServiceImpl implements AdministratorService {

    private PackageDao packageDao;
    private RouteDao routeDao;
    private CityDao cityDao;
    private UserDao userDao;

    public AdministratorServiceImpl() {
    }

    public AdministratorServiceImpl(PackageDao packageDao, RouteDao routeDao, CityDao cityDao, UserDao userDao) {
        this.packageDao = packageDao;
        this.routeDao = routeDao;
        this.cityDao = cityDao;
        this.userDao = userDao;
    }

    @WebMethod
    public int createPackage(Package pack) {
        pack.setSenderCity(cityDao.findCityByName(pack.getSenderCity().getName()));
        pack.setDetinationCity(cityDao.findCityByName(pack.getDetinationCity().getName()));
        pack.setSender(userDao.findUserByUsername(pack.getSender().getUsername()));
        pack.setReceiver(userDao.findUserByUsername(pack.getReceiver().getUsername()));
        return packageDao.savePackage(pack);
    }

    @WebMethod
    public void deletePackage(int id) {
        packageDao.deletePackage(id);
    }

    @WebMethod
    public void addRouteToPackage(int id, String date, String cityName) {
        try {
            Package pack = packageDao.findById(id);
            City city = cityDao.findCityByName(cityName);
            if (city == null) city = new City(cityName);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parsedDate = dateFormat.parse(date);
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            Route route = new Route(city, pack, timestamp);
            routeDao.saveRoute(route);
            if (!pack.isTracking()) {
                packageDao.setIsTracked(pack.getId());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @WebMethod
    public List<Package> getAllPackages() {
        return packageDao.listPackages();
    }

    @WebMethod
    public List<User> getAllUsers() {
        return userDao.findAll();
    }

    @WebMethod
    public List<City> getAllCities() {
        return cityDao.findAll();
    }

    @WebMethod
    public List<Package> findUserPackages(int id){
        return packageDao.findUserPackage(id);
    }

    @WebMethod
    public List<RouteDTO> findPackageRoutes(int id){
         List<Route> routes = routeDao.findRoutes(id);
         List<RouteDTO> dtos = new ArrayList<>();
         for(Route route:routes){
             RouteDTO routeDTO=new RouteDTO();
             routeDTO.setCity(route.getCity());
             routeDTO.setDate(route.getTime().toString());
             dtos.add(routeDTO);
         }
         return dtos;
    }
}
