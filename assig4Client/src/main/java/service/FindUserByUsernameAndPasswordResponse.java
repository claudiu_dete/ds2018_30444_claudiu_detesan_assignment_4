
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FindUserByUsernameAndPasswordResult" type="{http://service/}User" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "findUserByUsernameAndPasswordResult"
})
@XmlRootElement(name = "FindUserByUsernameAndPasswordResponse")
public class FindUserByUsernameAndPasswordResponse {

    @XmlElement(name = "FindUserByUsernameAndPasswordResult")
    protected User findUserByUsernameAndPasswordResult;

    /**
     * Gets the value of the findUserByUsernameAndPasswordResult property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getFindUserByUsernameAndPasswordResult() {
        return findUserByUsernameAndPasswordResult;
    }

    /**
     * Sets the value of the findUserByUsernameAndPasswordResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setFindUserByUsernameAndPasswordResult(User value) {
        this.findUserByUsernameAndPasswordResult = value;
    }

}
