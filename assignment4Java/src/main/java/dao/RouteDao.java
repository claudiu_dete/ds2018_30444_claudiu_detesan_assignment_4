package dao;

import entity.Route;
import factory.ConnectionFactory;
import org.hibernate.*;

import java.util.List;

public class RouteDao {

    private SessionFactory sessionFactory;
    private static final String FIND_USER_PACKAGE ="FROM Route WHERE package_id = :id";

    public RouteDao(){
        this.sessionFactory=ConnectionFactory.getInstance().getSessionFactory();
    }

    public int saveRoute(Route route){
        int id=0;
        try(Session session=sessionFactory.openSession()) {
            id = (int) session.save(route);
        } catch(HibernateException e){
            e.printStackTrace();
        }
        return id;
    }

    public List<Route> findRoutes(int id){
        Transaction tx=null;
        List routes=null;
        try(Session session=sessionFactory.openSession()) {
            tx=session.beginTransaction();
            Query query = session.createQuery(FIND_USER_PACKAGE);
            query.setParameter("id",id);
            routes = query.list();
            tx.commit();
        } catch(HibernateException e){
            if( tx !=null) tx.rollback();
            e.printStackTrace();

        }
        return routes;

    }




}
