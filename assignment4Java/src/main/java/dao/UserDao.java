package dao;

import entity.User;
import factory.ConnectionFactory;
import org.hibernate.*;

import java.util.List;

public class UserDao {

    private SessionFactory sessionFactory;
    private static final String FIND_USER = "FROM User WHERE username= :username";

    public UserDao() {
        this.sessionFactory = ConnectionFactory.getInstance().getSessionFactory();
    }

    public int createUser(User user) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int userId = -1;
        try {
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return userId;
    }

    public User findUserByUsername(String username) {
            Session session=sessionFactory.openSession();
            Query query = session.createQuery(FIND_USER);
            query.setParameter("username", username);
            User user = (User) query.uniqueResult();
            session.close();
            return user;
    }

    public List<User> findAll() {
        List users = null;
        try (Session session = sessionFactory.openSession()) {
            users = session.createCriteria(User.class).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return users;

    }
}
