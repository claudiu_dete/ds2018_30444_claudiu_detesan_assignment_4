﻿using ServerAssig4.entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ServerAssig4 
{
    public class TrackingContext : DbContext
    {
    public TrackingContext() : base(nameOrConnectionString: "MonkeyFist") { }

        public DbSet<User> User { get; set; }
        public DbSet<City> City { get; set; }
       
    }
}