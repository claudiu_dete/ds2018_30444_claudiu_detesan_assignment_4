package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import service.WebService1;
import service.WebService1Soap;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RegisterController implements Initializable {

    private WebService1Soap webService1Soap;

    @FXML
    private Button backBtn;
    @FXML
    private TextField usernameField;
    @FXML
    private TextField passwordField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            WebService1 webService1=new WebService1();
            webService1Soap = webService1.getWebService1Soap();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void back() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/LoginView.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage window =(Stage)backBtn .getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    public void register(){
        String username=usernameField.getText();
        String password=passwordField.getText();
        if(username.isEmpty() || password.isEmpty()){
            showAlert("Please enter both fields");
        }
        else{
            webService1Soap.createUser(username,password);
            showAlert("User registered");
        }
    }

    private void showAlert(String s) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(s);
        alert.showAndWait();
    }
}
