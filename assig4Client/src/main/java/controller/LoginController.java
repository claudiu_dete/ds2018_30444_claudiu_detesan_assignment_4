package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import service.WebService1;
import service.WebService1Soap;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    @FXML private TextField nameField;
    @FXML private TextField passField;
    @FXML private Button registerBtn;
    @FXML private Button loginBtn;

    private WebService1Soap webService1Soap;

    private static final String ADMINISTRATOR="ADMINISTRATOR";
    private static final String CLIENT="CLIENT";
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeController();
    }

    private void initializeController() {
        try {
            WebService1 webService1=new WebService1();
            webService1Soap = webService1.getWebService1Soap();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void login(){
        String username = nameField.getText();
        String password= passField.getText();
        service.User user =webService1Soap.findUserByUsernameAndPassword(username,password);
        if(user==null) showAlert("Invalid log in!");
        else{
            if(ADMINISTRATOR.equals(user.getRole())){
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/AdministratorView.fxml"));
                    Parent root = loader.load();
                    Scene scene = new Scene(root);
                    Stage window = (Stage) loginBtn.getScene().getWindow();
                    window.setScene(scene);
                    window.show();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }
            else{
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/ClientView.fxml"));
                    ClientController clientController = new ClientController(user.getId());
                    loader.setController(clientController);
                    Parent root = loader.load();
                    Scene scene = new Scene(root);
                    Stage window = (Stage) loginBtn.getScene().getWindow();
                    window.setScene(scene);
                    window.show();
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }
        }

    }

    public void register() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/RegisterView.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage window =(Stage)registerBtn .getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    private void showAlert(String s) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(s);
        alert.showAndWait();
    }


}
