package dao;

import entity.City;
import factory.ConnectionFactory;
import org.hibernate.*;

import java.util.List;

public class CityDao {

    private final SessionFactory sessionFactory;

    private static final String FIND_CITY = "from City WHERE name= :name";

    public CityDao(){
        this.sessionFactory=ConnectionFactory.getInstance().getSessionFactory();
    }

    public City findCityByName(String name) {
        Transaction tx = null;
        City city = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Query query = session.createQuery(FIND_CITY);
            query.setParameter("name", name);
            city = (City) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return city;
    }

    public List<City> findAll() {
        List cities = null;
        try (Session session = sessionFactory.openSession()) {
            cities = session.createCriteria(City.class).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return cities;

    }

}
