package entity;


import java.sql.Timestamp;

public class Route {

    private int id;
    private City city;
    private Package routedPackage;
    private Timestamp time;
    public Route() {
    }

    public Package getaPackage() {
        return routedPackage;
    }

    public void setaPackage(Package aPackage) {
        this.routedPackage = aPackage;
    }

    public Route(City city, Package routedPackage, Timestamp time) {
        this.city = city;
        this.routedPackage = routedPackage;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
