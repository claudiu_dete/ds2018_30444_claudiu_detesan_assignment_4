﻿using ServerAssig4.dao;
using ServerAssig4.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ServerAssig4
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://service/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public User  FindUserByUsernameAndPassword(String username,String password)
        {
            UserDao Userdao = new UserDao();
            return Userdao.FindByUsernameAndPassword( username,  password);
        }

        [WebMethod]

        public void CreateUser(String username,String password)
        {
            UserDao Userdao = new UserDao();
            User user = new entity.User(username, password, "CLIENT");
            Userdao.CreateUser(user);
        }

       


    }
}
