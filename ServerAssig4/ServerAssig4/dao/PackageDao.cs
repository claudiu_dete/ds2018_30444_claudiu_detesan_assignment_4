﻿using ServerAssig4.entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ServerAssig4.dao
{
    public class PackageDao
    {

        protected TrackingContext _dbContext;
        protected DbSet<Package> _dbSet;

        public PackageDao()
        {
            _dbContext = new TrackingContext();
            _dbSet = _dbContext.Packages;
        }

        public List<Package> FindAllUserPackages(int id)
        {
             return _dbSet.Include(p => p.Sender)
                     .Include(p => p.Receiver)
                     .Include(p => p.Sender)
                     .Include(p => p.SenderCity)
                     .Include(p => p.DestinationCity)
                     .Where(p => p.Sender.Id == id || p.Receiver.Id == id).ToList();
            
        }
    }
}