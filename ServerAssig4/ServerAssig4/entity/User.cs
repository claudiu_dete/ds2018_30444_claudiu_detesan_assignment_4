﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServerAssig4.entity
{
    [Table("user")]
    public class User
    {

        [Key]
        public int Id { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Role { get; set; }

        public User()
        {

        }

        public User(String username,String password,String role)
        {
            Username = username;
            Password = password;
            Role = role;
        }

       
    }
}