package controller;


import entity.Package;
import entity.PackageTable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import service.AdministratorService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AdministratorController implements Initializable {

    private AdministratorService administratorService;

    @FXML private TableView<PackageTable> tablePackage;
    @FXML private TableColumn<PackageTable,String> tableId;
    @FXML private TableColumn<PackageTable,String> tableSender;
    @FXML private TableColumn<PackageTable,String> tableReceiver;
    @FXML private TableColumn<PackageTable,String> tableName;
    @FXML private TableColumn<PackageTable,String> tableDescription;
    @FXML private TableColumn<PackageTable,String> tableCity;
    @FXML private TableColumn<PackageTable,String> tableTracking;
    @FXML private TextField idField;
    @FXML private TextField cityField;
    @FXML private DatePicker dateField;
    @FXML private Button addBtn;

    public AdministratorController() {

    }

    public void initialize(URL location, ResourceBundle resources) {
        initializeController();
        initTable();
    }

    private void initializeController() {
        try {
            URL url = new URL("http://localhost:8080/administratorService?wsdl");
            QName qname = new QName("http://service/", "AdministratorServiceImplService");
            Service service = Service.create(url, qname);
            administratorService = service.getPort(AdministratorService.class);
            } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deletePackage(){
        try {
            int id = Integer.parseInt(idField.getText());
            administratorService.deletePackage(id);
            initTable();
        }
        catch(NumberFormatException e){
            showAlert("Id must be a number");
        }
    }

    private void initTable(){
        tableId.setCellValueFactory(new PropertyValueFactory<PackageTable,String>("id"));
        tableSender.setCellValueFactory(new PropertyValueFactory<PackageTable,String>("sender"));
        tableReceiver.setCellValueFactory(new PropertyValueFactory<PackageTable,String>("receiver"));
        tableName.setCellValueFactory(new PropertyValueFactory<PackageTable,String>("name"));
        tableDescription.setCellValueFactory(new PropertyValueFactory<PackageTable,String>("description"));
        tableCity.setCellValueFactory(new PropertyValueFactory<PackageTable,String>("city"));
        tableTracking.setCellValueFactory(new PropertyValueFactory<PackageTable,String>("tracking"));
        List<Package> packageList=administratorService.getAllPackages();
        List<PackageTable> tablePackages = new ArrayList<PackageTable>();
        for(Package pack:packageList){
            PackageTable packageTable=new PackageTable();
            packageTable.setId(String.valueOf(pack.getId()));
            packageTable.setSender(pack.getSender().getUsername());
            packageTable.setReceiver(pack.getReceiver().getUsername());
            packageTable.setName(pack.getName());
            packageTable.setDescription(pack.getDescription());
            packageTable.setCity(pack.getSenderCity().getName()+" "+pack.getDetinationCity().getName());
            packageTable.setTracking(String.valueOf(pack.isTracking()));
            tablePackages.add(packageTable);
        }
        tablePackage.getItems().setAll(tablePackages);
    }

    public void addPackage() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/AddPackageView.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage window =(Stage)addBtn .getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    public void addRoute(){
        try {
            int id = Integer.parseInt(idField.getText());
            String cityName=cityField.getText();
            String date=dateField.getValue().toString();
            administratorService.addRouteToPackage(id,date,cityName);
            showAlert("Package Added");
        }
        catch(NumberFormatException e){
            showAlert("Id must be a number");
        }
    }

    private void showAlert(String s) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(s);
        alert.showAndWait();
    }
}

