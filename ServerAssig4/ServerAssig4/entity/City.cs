﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServerAssig4.entity
{
    [Table("city")]
    public class City
    {
        
        [Key]
        public int Id { get; set; }
        public String Name { get; set; }

        public City()
        {

        }

    }
}