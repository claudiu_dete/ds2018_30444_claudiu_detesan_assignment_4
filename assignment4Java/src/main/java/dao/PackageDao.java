package dao;

import entity.Package;
import factory.ConnectionFactory;
import org.hibernate.*;
import java.util.List;


public class PackageDao {

    private SessionFactory sessionFactory;
    public static final String DELETE_PACKAGE = "DELETE FROM Package WHERE id= :id";
    public static final String UPDATE_TRACKED = "UPDATE Package SET tracking = true WHERE id= :id";
    private static final String FIND_PACKAGE = "FROM Package WHERE id= :id";
    private static final String FIND_USER_PACKAGE ="FROM Package WHERE (sender_id= :id1 OR receiver_id= :id2)";

    public PackageDao() {
        this.sessionFactory=ConnectionFactory.getInstance().getSessionFactory();
        }

    public List<Package> listPackages(){
        List packages = null;
        try (Session session = sessionFactory.openSession()) {
            packages = session.createCriteria(Package.class).list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return packages;
    }

    public int savePackage(Package pack){
        int id=0;
        try(Session session=sessionFactory.openSession()) {
            id = (int) session.save(pack);
        } catch(HibernateException e){
            e.printStackTrace();
        }
        return id;
    }

    public void deletePackage(int id){
        Transaction tx = null;
        try(Session session = sessionFactory.openSession()) {
            tx=session.beginTransaction();
            Query query = session.createQuery(DELETE_PACKAGE);
            query.setParameter("id",id);
            query.executeUpdate();
            tx.commit();
        } catch(HibernateException e) {
            if( tx !=null) tx.rollback();
            e.printStackTrace();
        }
    }

    public void setIsTracked(int id){
        Transaction tx=null;
        try(Session session=sessionFactory.openSession()) {
            tx=session.beginTransaction();
            Query query = session.createQuery(UPDATE_TRACKED);
            query.setParameter("id",id);
            query.executeUpdate();
            tx.commit();
        } catch(HibernateException e){
            if( tx !=null) tx.rollback();
            e.printStackTrace();

        }
    }

    public Package findById(int id){
        Session session=sessionFactory.openSession();
        Query query = session.createQuery(FIND_PACKAGE);
        query.setParameter("id", id);
        Package pack = (Package) query.uniqueResult();
        session.close();
        return pack;
    }

    public List<Package> findUserPackage(int userId){
        Transaction tx=null;
        List packages=null;
        try(Session session=sessionFactory.openSession()) {
            tx=session.beginTransaction();
            Query query = session.createQuery(FIND_USER_PACKAGE);
            query.setParameter("id1",userId);
            query.setParameter("id2",userId);
            packages = query.list();
            tx.commit();
        } catch(HibernateException e){
            if( tx !=null) tx.rollback();
            e.printStackTrace();

        }
        return packages;
    }

}
