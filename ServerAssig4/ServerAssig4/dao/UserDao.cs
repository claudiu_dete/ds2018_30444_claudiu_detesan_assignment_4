﻿using ServerAssig4.entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ServerAssig4.dao
{

    public class UserDao
    {
        protected TrackingContext _dbContext;
        protected DbSet<User> _dbSet;

        public UserDao()
        {
            _dbContext = new TrackingContext();
            _dbSet = _dbContext.User;
        }

        public void CreateUser(User user)
        {
            _dbSet.Add(user);
            SaveChanges();
        }

        public User FindByUsernameAndPassword(String username,String password)
        {
            var user = _dbSet.Where(u => u.Username == username && u.Password ==password).FirstOrDefault();
            return user;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }


    }
}