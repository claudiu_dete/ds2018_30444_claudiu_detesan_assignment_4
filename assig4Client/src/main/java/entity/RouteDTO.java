package entity;

public class RouteDTO {

    private City city;
    private String date;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RouteDTO() {
    }
}
